import plotly.express as px
from all_dataframes import CoronaDfs
from datetime import date


class CoronaGraphs:
    def __init__(self, time_to_sample='D', start_date=date(2020, 3, 11), end_date=date.today()):
        self.data = CoronaDfs(time_to_sample, start_date, end_date)

    def create_tests_graph(self):
        title = 'New testers per time'
        tests_data = self.data.tests_per_time
        return px.line(tests_data, title=title)

    def create_positive_graph(self):
        title = 'New corona cases per time'
        positive_data = self.data.positive_per_time
        return px.line(positive_data, title=title)

    def create_percent_graph(self):
        title = 'Positive percent from all testers per time'
        percent_data = self.data.percent_per_time
        return px.line(percent_data, title=title)


if __name__ == '__main__':
    cg = CoronaGraphs('D')
    cg.create_tests_graph()
