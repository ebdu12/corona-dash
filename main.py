import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from coronagraphs import CoronaGraphs
from datetime import date, datetime
import html_parts as h

app = dash.Dash()
colors = {
    'background': 'green',
    'text': '#7FDBFF'
}
app.layout = html.Div(style={'backgroundColor': colors['background'], 'color': colors['text']}, children=[
    h.WelcomeHtml,
    h.SubTitle,
    dcc.DatePickerRange(id='my-date-picker-range',
                        min_date_allowed=date(2020, 3, 11),
                        max_date_allowed=date.today(),
                        start_date=date(2020, 3, 11),
                        end_date=date.today()),

    html.Div(id='container')
])


@app.callback(
    Output('container', 'children'),
    [dash.dependencies.Input('my-date-picker-range', 'start_date'),
     dash.dependencies.Input('my-date-picker-range', 'end_date')])
def display(start_date, end_date):
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    g = CoronaGraphs('W', start_date, end_date)
    return html.Div([
        dcc.Graph(id='tests_graph', figure=g.create_tests_graph()),
        dcc.Graph(id='positive_graph', figure=g.create_positive_graph()),
        dcc.Graph(id='percent_graph', figure=g.create_percent_graph())
    ])


if __name__ == '__main__':
    app.run_server(debug=True)
