import pandas as pd
import requests as r
import datetime as dt

date_range = pd.date_range('03-11-2020', dt.datetime.now())

api_obj = r.api

df_cols = ['_id', 'result_date', 'corona_result', 'lab_id', 'test_for_corona_diagnosis', 'is_first_Test']

main_df = pd.DataFrame(columns=df_cols)
for date in date_range:
    str_date = date.strftime('%Y-%m-%d')
    print(str_date)
    url = f"https://data.gov.il/api/3/action/datastore_search?resource_id=dcf999c1-d394-4b57-a5e0-9d014a62e046&limit=500000&filters={{%22result_date%22:%22{str_date}%22}}"
    api_results = api_obj.get(url)
    json_results = api_results.json()['result']['records']
    one_date_df = pd.DataFrame(json_results)
    main_df = pd.concat([main_df, one_date_df], ignore_index=True)

summaries_df = main_df.groupby([col for col in df_cols if col != '_id']).count().rename(
    columns={'_id': 'counter'}).reset_index()


english_dict = {'שלילי':'negetive'
,'לא ודאי ישן':'unknown old'
, 'חיובי': 'positive'
, 'בעבודה': 'at work'
, 'חיובי גבולי': 'almost positive'
, 'חיובי באיגום': 'almost positive'
, 'חיובי גבולי באיגום': 'almost positive'
, 'לא בוצע – איגום': 'didnt happen'
, 'חיובי אנטיגן': 'positive antigen'
, 'ראו הערה': 'unknown'
, 'חיובי סרולוגיה': 'serology positive'
, 'לא בוצע/פסול 999': 'didnt happen'
, 'בדיקה לא הגיעה למעבדה': 'didnt happen'
 }
summaries_df['corona_result'] = summaries_df['corona_result'].apply(lambda x: english_dict[x] if x in english_dict.keys() else 'A new value!')

summaries_df.to_csv(r'../db/sum_tests_per_day.csv', index=False)
print('finished')
