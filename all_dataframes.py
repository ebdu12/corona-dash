import pandas as pd


class CoronaDfs:
    def __init__(self, time_to_sample, start_date, end_date):
        self._time_to_sample = time_to_sample
        self._start_date = start_date
        self._end_date = end_date
        self.tests_per_time = None
        self.positive_per_time = None
        self.percent_per_time = None
        self.add_all_dfs()

    def add_all_dfs(self):
        self._add_tests_df()
        self.create_tests_ser()
        self.create_positive_ser()
        self.create_percent_ser()

    def create_tests_ser(self):
        operator = self._tests_sum_df['is_first_Test'] == 'Yes'
        self.tests_per_time = self.create_wanted_data(self._tests_sum_df, operator)
        self.tests_per_time.name = 'tests_counter'

    def create_positive_ser(self):
        operator = (self._tests_sum_df['corona_result'] == 'positive') & (
                self._tests_sum_df['test_for_corona_diagnosis'] == 1)
        self.positive_per_time = self.create_wanted_data(self._tests_sum_df, operator)
        self.positive_per_time.name = 'positive_counter'

    def create_percent_ser(self):
        pos_and_tests_df = pd.merge(self.tests_per_time, self.positive_per_time, how='left', left_index=True,
                                    right_index=True)
        pos_and_tests_df['precent'] = pos_and_tests_df['positive_counter'] / pos_and_tests_df['tests_counter'] * 100
        self.percent_per_time = pos_and_tests_df['precent']

    def create_wanted_data(self, df, operator):
        date_counter_data = df[operator].resample(self._time_to_sample).sum()['counter']
        return date_counter_data

    def _add_tests_df(self):
        self._tests_sum_df = pd.read_csv('./db/sum_tests_per_day.csv')
        self._tests_sum_df['result_date'] = pd.to_datetime(self._tests_sum_df['result_date'], format='%d/%m/%Y')
        mask = (self._tests_sum_df['result_date'] > self._start_date) & (
                    self._tests_sum_df['result_date'] < self._end_date)
        self._tests_sum_df = self._tests_sum_df[mask]
        self._tests_sum_df = self._tests_sum_df.set_index('result_date')
