import dash
import dash_core_components as dcc
import dash_html_components as html

WelcomeHtml = html.H1(
        children='Hello Dash',
        style={
            'textAlign': 'center',
        }
    )

SubTitle = html.Div(children='Dash: A web application framework for Python.', style={
        'textAlign': 'center'
    })